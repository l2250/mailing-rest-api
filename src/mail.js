"use strict";
require("dotenv").config();
const nodemailer = require("nodemailer");

const sendEmail = async (mailObj) => {
    const { to, subject, message } = mailObj;

    try {
        // Create a transporter
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASS,
            },
        });

        // Send mail with defined transport object
        let info = await transporter.sendMail({
            from: process.env.MAIL_USER,
            to: to,
            subject: subject,
            html: message,
        });

        console.log(`Message sent: ${info.messageId}`);
        return `Message sent: ${info.messageId}`;
    } catch (error) {
        console.error(error);
        throw new Error(
            `Something went wrong in the sendmail method. Error: ${error.message}`
        );
    }
};

module.exports = sendEmail;