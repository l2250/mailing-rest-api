"use strict";
const path = require("path");
const notifier = require('node-notifier');

const sendNotification = async (notificationObj) => {
    const { title, message } = notificationObj;

    if (!title || !message) {
        throw new Error(
            `Something went wrong in the sendNotification method. Error: title and message must be filled`
        );
    }

    console.log(path.join(__dirname, '../icon.png'))
    var notifierOption = {
        title: title,
        message: message,
        icon: path.join(__dirname, '../assets/icon.png'),
    }
    notifier.notify(notifierOption, function (error, response) {
        if (error) {
            console.log(error);
            throw new Error(
                `Something went wrong in the sendNotification method. Error: ${error}`
            );
        } else {
            console.log("Notification sent: " + response);
            return "Notification has been sent";
        }
    });
}

module.exports = sendNotification;