"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const mail = require("./routes/mail.js");
const notification = require("./routes/notification.js");

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Extended: https://swagger.io/specification/#infoObject

const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: "Mailing and Notification Rest API",
            version: '1.0.0',
            description: "This is a simple mailing API.",
            termsOfService: "http://example.com/terms",
            contact: {
                name: "Les ambassadeurs"
            },
            servers: ["http://localhost:4444"]
        }
    },
    apis: ["./routes/mail.js", "./routes/notification.js"]
};

// Express body parser
app.use(cors());
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        limit: "50mb",
        extended: false,
        parameterLimit: 50000
    })
);

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
// Use the routes specified in route folder
app.use("/api/v1/mail", mail);
app.use("/api/v1/notification", notification);

const port = process.env.PORT || 4444;

// Listen to the server
app.listen(port, function () {
    console.log(`Listening to the port ${port} .....`);
});