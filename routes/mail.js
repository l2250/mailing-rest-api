const express = require("express");
const router = express.Router();
const sendMailMethod = require("../src/mail");

// Post request to send an email
/**
 * @openapi
 * /api/v1/mail/send:
 *   post:
 *     summary: Sends an email
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - to
 *               - subject
 *               - message
 *             properties:
 *               to:
 *                 type: array
 *                 items:
 *                   type: string
 *                 definition: Array of recipient addresses
 *               subject:
 *                 type: string
 *                 definition: Subject of message
 *               message:
 *                 type: string
 *                 definition: Content of message
 *           example:
 *             to: ["test@mail.com"]
 *             subject: "My subject"
 *             message: "My message"
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: Bad request. Error message in payload.
 */
router.post("/send", async (req, res) => {
    try {
        const result = await sendMailMethod(req.body);

        // Send the response
        res.status(200).json({
            status: true,
            payload: result,
        });
    } catch (error) {
        console.error(error.message);
        res.status(400).json({
            status: false,
            payload: error.message,
        });
    }
});

module.exports = router;