const express = require("express");
const router = express.Router();
const sendNotificationMethod = require("../src/notification");

// Post request to send a notification
/**
 * @openapi
 * /api/v1/notification/send:
 *   post:
 *     summary: Sends a desktop notification
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - title
 *               - message
 *             properties:
 *               title:
 *                 type: string
 *                 definition: Title of notification
 *               message:
 *                 type: string
 *                 definition: Content of notification
 *           example:
 *             title: "My title"
 *             message: "My message"
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: Bad request. All parameters must have a value.
 */
router.post("/send", async (req, res) => {
    try {
        const result = await sendNotificationMethod(req.body);

        // Send the response
        res.status(200).json({
            status: true,
            payload: result,
        });
    } catch (error) {
        console.error(error.message);
        res.status(400).json({
            status: false,
            payload: error.message,
        });
    }
});

module.exports = router;